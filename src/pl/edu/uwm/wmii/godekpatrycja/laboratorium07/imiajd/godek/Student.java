package pl.edu.uwm.wmii.godekpatrycja.laboratorium07.imiajd.godek;

public class Student extends Osoba{
    private String kierunek;
    public Student(String nazwisko, String rokUrodzenia, String kierunek){
        super(nazwisko,rokUrodzenia);
        this.kierunek=kierunek;
    }

    public String getKierunek() {
        return this.kierunek;
    }

    @Override
    public String toString() {
        return getNazwisko()+" "+ getRokUrodzenia()+" "+getKierunek();
    }
}