package pl.edu.uwm.wmii.godekpatrycja.laboratorium07.imiajd.godek;

public class Osoba {

    public Osoba(String nazwisko, String rokUrodzenia)
    {
        this.nazwisko=nazwisko;
        this.rokUrodzenia=rokUrodzenia;
    }
    public String getNazwisko()
    {
        return this.nazwisko;
    }
    public String getRokUrodzenia()
    {
        return this.rokUrodzenia;
    }
    @Override
    public String toString()
    {
        return getNazwisko()+" "+getRokUrodzenia();
    }
    private String nazwisko;
    private String rokUrodzenia;
}