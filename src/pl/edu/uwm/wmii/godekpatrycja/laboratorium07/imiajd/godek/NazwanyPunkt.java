package pl.edu.uwm.wmii.godekpatrycja.laboratorium07.imiajd.godek;

public class NazwanyPunkt extends pl.imiajd.godek.Punkt {
    NazwanyPunkt(int a, int b, String var) {
        super(a, b);
        this.name = var;
    }

    public void show() {
        System.out.println(this.name + ":<" + this.x() + ", " + this.y() + ">");
    }
    private String name;
}
