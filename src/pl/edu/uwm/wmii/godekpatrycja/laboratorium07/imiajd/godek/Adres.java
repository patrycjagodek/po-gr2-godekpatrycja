package pl.edu.uwm.wmii.godekpatrycja.laboratorium07.imiajd.godek;

public class Adres {
    private String ulica;
    private int numer_domu;
    private int numer_mieszkania;
    private String miasto;
    private String kod_pocztowy;

    public Adres(String ulica, int numer_domu, int numer_mieszkania, String miasto, String kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania=numer_mieszkania;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public Adres(String ulica, int numer_domu, String miasto, String kod_pocztowy)
    {
        this.ulica=ulica;
        this.numer_domu=numer_domu;
        this.numer_mieszkania= 0;
        this.miasto=miasto;
        this.kod_pocztowy=kod_pocztowy;
    }
    public void pokaz()
    {
        System.out.format("%s, %s\n", this.kod_pocztowy, this.miasto);
        if (this.numer_mieszkania!=0)
        {
            System.out.format("%s %d/%d\n", this.ulica, this.numer_domu, this.numer_mieszkania);
        }
        else
        {
            System.out.format("%s %d", this.ulica, this.numer_domu);
        }
    }
    public boolean przed(Adres inny)
    {
        for (int i = 0; i < inny.kod_pocztowy.length(); i++)
        {
            if (inny.kod_pocztowy.charAt(i)>this.kod_pocztowy.charAt(i))
            {
                return true;
            }
        }
        return false;
    }
}
