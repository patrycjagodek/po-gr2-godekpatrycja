package pl.edu.uwm.wmii.godekpatrycja.laboratorium07.imiajd.godek;

public class Nauczyciel extends Osoba{
    private int pensja;
    public Nauczyciel(String nazwisko, String rokUrodzenia, int pensja){
        super(nazwisko,rokUrodzenia);
        this.pensja=pensja;
    }

    public int getPensja() {
        return this.pensja;
    }

    @Override
    public String toString() {
        return "Nazwisko: "+getNazwisko()+"\nRok urodzenia: "+getRokUrodzenia()+"\nPensja: "+getPensja();
    }
}