package pl.edu.uwm.wmii.godekpatrycja.laboratorium10.imiajd.godek;
import java.time.LocalDate;

public class Osoba implements Cloneable, Comparable<Osoba>{
    public Osoba(String nazwisko, LocalDate DataUrodzenia){
        this.nazwisko=nazwisko;
        this.DataUrodzenia=DataUrodzenia;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public LocalDate getDataUrodzenia() {
        return this.DataUrodzenia;
    }

    @Override
    public boolean equals(Object obj) {
        Osoba osoba = (Osoba) obj;
        return (osoba.nazwisko.equals(this.nazwisko) && osoba.DataUrodzenia.equals(this.DataUrodzenia));
    }

    @Override
    public String toString() {
        return "[" + this.nazwisko+", "+this.DataUrodzenia.toString() + "]";
    }

    @Override
    public int compareTo(Osoba osoba) {
        int comp = this.nazwisko.compareTo(osoba.nazwisko);
        if(comp == 0){
            return this.DataUrodzenia.compareTo(osoba.DataUrodzenia);
        }
        return comp;
    }

    private String nazwisko;
    private LocalDate DataUrodzenia;
}