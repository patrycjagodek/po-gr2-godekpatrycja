package pl.edu.uwm.wmii.godekpatrycja.laboratorium10.imiajd.godek;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Zad3 {
    public static void main(String[] args) {
            ArrayList<String> zad3 = new ArrayList<>();
            try {
                File plik = new File("src/pl/edu/uwm/wmii/godekpatrycja/laboratorium10/imiajd/godek/plik.txt");
                Scanner zczytywanie = new Scanner(plik);
                while (zczytywanie.hasNextLine()) {
                    zad3.add(zczytywanie.nextLine());
                }
                zczytywanie.close();
            } catch (FileNotFoundException e) {
                System.out.println("Plik nie istnieje");
                e.printStackTrace();
            }

            System.out.println(zad3);
            Collections.sort(zad3);
            System.out.println(zad3);
        }
    }
