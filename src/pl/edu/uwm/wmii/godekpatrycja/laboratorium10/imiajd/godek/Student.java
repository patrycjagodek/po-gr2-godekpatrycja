package pl.edu.uwm.wmii.godekpatrycja.laboratorium10.imiajd.godek;
import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable<Osoba> {
    public Student(String nazwisko, LocalDate DataUrodzenia, double sredniaOcen) {
        super(nazwisko, DataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    private double sredniaOcen;

    @Override
    public String toString() {
        return "[" + this.getNazwisko() + ", " + this.getDataUrodzenia().toString() + ", " + this.sredniaOcen + "]";
    }

    @Override
    public int compareTo(Osoba osoba) {
        int comp = super.compareTo(osoba);
        if (comp == 0 && osoba instanceof Student) {
            if (this.sredniaOcen > ((Student) osoba).sredniaOcen) {
                return 1;
            }
            if (this.sredniaOcen == ((Student) osoba).sredniaOcen) {
                return 0;
            }
            if (this.sredniaOcen < ((Student) osoba).sredniaOcen) {
                return -1;
            }
        }
        return comp;
    }
}