package pl.edu.uwm.wmii.godekpatrycja.laboratorium10.imiajd.godek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Osoba> lista = new ArrayList<>(4);
        lista.add(new Student("Godek", LocalDate.of(1999, 7, 26), 5.00));
        lista.add(new Student("Bodek", LocalDate.of(1998, 9, 20), 4.00));
        lista.add(new Student("Bodek", LocalDate.of(1997, 8, 12), 4.50));
        lista.add(new Student("Badek", LocalDate.of(1998, 9, 20), 5.00));

        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);
    }
}
