package pl.edu.uwm.wmii.godekpatrycja.laboratorium10.imiajd.godek;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {
    public static void main(String[] args){
        ArrayList<Osoba> lista = new ArrayList<>(4);
        lista.add(new Osoba("Godek", LocalDate.of(1999, 7, 26)));
        lista.add(new Osoba("Bodek", LocalDate.of(1998, 9, 20)));
        lista.add(new Osoba("Bodek", LocalDate.of(1997, 8, 12)));
        lista.add(new Osoba("Badek", LocalDate.of(1998, 9, 20)));
        System.out.println(lista);
        Collections.sort(lista);
        System.out.println(lista);
    }
}
