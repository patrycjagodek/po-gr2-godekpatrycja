package pl.edu.uwm.wmii.godekpatrycja.laboratorium06;

public class RachunekBankowy {

    public static double rocznaStopaProcentowa;
    private double saldo;
    public RachunekBankowy(double saldo)
    {
        this.saldo=saldo;
    }
    public double obliczMiesieczneOdsetki()
    {
        this.saldo+=(this.saldo*rocznaStopaProcentowa)/12;
        return (this.saldo*rocznaStopaProcentowa)/12;
    }
    public static void setRocznaStopaProcentowa(double nowa_rocznaStopaProcentowa)
    {
        rocznaStopaProcentowa=nowa_rocznaStopaProcentowa;
    }
    public double getSaldo()
    {
        return this.saldo;
    }


    public static void main(String[] args) {
        RachunekBankowy saver1=new RachunekBankowy(2000);
        RachunekBankowy saver2=new RachunekBankowy(3000);
        RachunekBankowy.setRocznaStopaProcentowa(0.04);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
        RachunekBankowy.setRocznaStopaProcentowa(0.05);
        saver1.obliczMiesieczneOdsetki();
        saver2.obliczMiesieczneOdsetki();
        System.out.println(saver1.getSaldo());
        System.out.println(saver2.getSaldo());
    }
}
