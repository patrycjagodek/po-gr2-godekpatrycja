package pl.edu.uwm.wmii.godekpatrycja.laboratorium06;

public class IntegerSet {
    private final boolean[] tab;

    public IntegerSet() {
        this.tab = new boolean[100];
    }

    public static IntegerSet union(IntegerSet z1, IntegerSet z2) {
        IntegerSet wynik = new IntegerSet();
        for (int i = 0; i < 100; i++) {
            if (z1.tab[i] || z2.tab[i]) {
                wynik.tab[i] = true;
            }
        }
        return wynik;
    }

    public static IntegerSet intersection(IntegerSet z1, IntegerSet z2) {
        IntegerSet wynik = new IntegerSet();
        for (int i = 0; i < 100; i++) {
            if (z1.tab[i] && z2.tab[i]) {
                wynik.tab[i] = true;
            }
        }
        return wynik;
    }

    public void insertElement(int n) {
        this.tab[n - 1] = true;
    }

    public void deleteElement(int n) {
        this.tab[n - 1] = false;
    }

    @Override
    public String toString() {
        StringBuilder wynik = new StringBuilder();
        for (int i = 0; i < 100; i++) {
            if (this.tab[i]) {
                wynik.append(i + 1 + " ");
            }
        }
        return wynik.toString();
    }

    public boolean equals(IntegerSet zbior2) {
        return this.toString().equals(zbior2.toString());
    }

    public static void main(String[] args) {
        IntegerSet x1 = new IntegerSet();
        IntegerSet x2 = new IntegerSet();

        x1.insertElement(4);
        x1.insertElement(3);
        x1.insertElement(7);
        x1.insertElement(8);
        x1.insertElement(1);
        x2.insertElement(5);
        x2.insertElement(10);
        x2.insertElement(9);
        x2.insertElement(2);
        x2.insertElement(6);

        System.out.println(union(x1, x2));
        System.out.println(intersection(x1, x2));
        x2.deleteElement(5);
        System.out.println(x2);
        System.out.println(x1.equals(x2));
        IntegerSet x3= new IntegerSet();
        x3.insertElement(10);
        x3.insertElement(9);
        x3.insertElement(2);
        x3.insertElement(6);

        System.out.println(x2.equals(x3));
    }
}