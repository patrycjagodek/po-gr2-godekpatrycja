package pl.edu.uwm.wmii.godekpatrycja.test;

import java.util.*;

public class Test {

    public static void isPrime(int n) {
        float[] tab = new float[n];
        Scanner scan = new Scanner(System.in);
        System.out.println("Wprowadz liczby:");
        for (int i = 0; i < n; i++) {
            tab[i] = scan.nextFloat();
        }
        for (int i = 0;i < n; i++){
            boolean prime = true;
            if(tab[i] <= 1){
                prime = false;
            }
            for(int j = 2; j < tab[i] ;j++){
                if(tab[i] % j == 0){
                    prime = false;
                }
            }
            if(prime){
                System.out.println(tab[i]);
            }
        }
    }

public static ArrayList<Integer> merge(ArrayList<Integer> tab1, ArrayList<Integer> tab2){
    int min=Math.min(tab1.size(), tab2.size());
    int max=Math.max(tab1.size(), tab2.size());
    ArrayList<Integer> tabL =new ArrayList<>();
    for (int i=0; i<min;i++)
    {
        tabL.add(tab1.get(i));
        tabL.add(tab2.get(i));
    }
    for (int i=min; i<max;i++)
    {
        if (tab1.size()>tab2.size())
        {
            tabL.add(tab1.get(i));
        }
        else
        {
            tabL.add(tab2.get(i));
        }
        }
    Set<Integer> hashSet = new LinkedHashSet(tabL);
    ArrayList<Integer> removedDuplicates = new ArrayList(hashSet);
    return removedDuplicates;
}

    public static void main(String[] args) {

        isPrime(3);
        ArrayList<Integer> tab1= new ArrayList<>(Arrays.asList(4,3,2,8,7,1));
        ArrayList<Integer> tab2= new ArrayList<>(Arrays.asList(1,7,3,6,5));
        System.out.println(tab1);
        System.out.println(tab2);
        System.out.println(merge(tab1,tab2));
    }
}
