package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;
import java.time.LocalDate;

public class TestOsoba
{
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];

        ludzie[0] = new Pracownik("Kowalski", new String[]{"Jan"}, LocalDate.of(1990,8,4), true, 4000.0, LocalDate.parse("2010-10-10"));
        ludzie[1] = new Student("Nowak", new String[]{"Małgorzata", "Monika"}, LocalDate.of(1989,1,2), false, "Informatyka", 4.25);
        // ludzie[2] = new Osoba("Dyl Sowizdrzał");

        for (Osoba p : ludzie) {
            for ( String i : p.getImiona()){
                System.out.print(i + " ");
            }
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
            if(p.isPlec()){
                System.out.println("Mężczyzna");
            }
            else {
                System.out.println("Kobieta");
            }
        }

    }
}
