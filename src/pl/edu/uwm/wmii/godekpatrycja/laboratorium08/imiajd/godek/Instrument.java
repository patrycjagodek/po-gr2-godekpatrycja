package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;
import java.time.LocalDate;

public abstract class Instrument {
    public Instrument(String producent, LocalDate rokProdukcji){
        this.producent=producent;
        this.rokProdukcji=rokProdukcji;
    }

    public abstract String dzwiek();

    public String getProducent() {
        return this.producent;
    }

    public LocalDate getRokProdukcji() {
        return this.rokProdukcji;
    }

    @Override
    public boolean equals(Object obj) {
        return this.toString().equals(obj.toString());
    }

    @Override
    public String toString() {
        return "Producent: " + producent + "\nrok produkcji: " + rokProdukcji;
    }

    private String producent;
    private LocalDate rokProdukcji;
}