package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;

import java.time.LocalDate;
public class Flet extends Instrument{
    public Flet(String producent, LocalDate rokProdukcji){
        super(producent,rokProdukcji);
    }

    public String dzwiek(){
        return "piri rim";
    }
}
