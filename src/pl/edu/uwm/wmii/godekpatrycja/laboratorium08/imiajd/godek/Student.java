package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;
import java.time.LocalDate;

class Student extends Osoba
{
    public Student(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec, String kierunek, double sredniaOcen)
    {
        super(nazwisko, imiona, DataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen= sredniaOcen;
    }

    public String getOpis()
    {
        return "kierunek: " + kierunek;
    }

    public double getSredniaOcen(){
        return sredniaOcen;
    }
    public void setSredniaOcen(double sredniaOcen){
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}