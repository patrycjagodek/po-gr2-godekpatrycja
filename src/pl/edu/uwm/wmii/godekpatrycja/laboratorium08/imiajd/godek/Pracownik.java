package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;
import java.time.LocalDate;

class Pracownik extends Osoba
{
    public Pracownik(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec, double pobory, LocalDate DataZatrudnienia)
    {
        super(nazwisko, imiona, DataUrodzenia, plec);
        this.pobory = pobory;
        this.DataZatrudnienia=DataZatrudnienia;
    }

    public double getPobory()
    {
        return pobory;
    }

    public String getOpis()
    {
        return String.format("pracownik z pensją %.2f zł", pobory);
    }

    public LocalDate getDataZatrudnienia()
    {
        return DataZatrudnienia;
    }

    private double pobory;
    private LocalDate DataZatrudnienia;
}