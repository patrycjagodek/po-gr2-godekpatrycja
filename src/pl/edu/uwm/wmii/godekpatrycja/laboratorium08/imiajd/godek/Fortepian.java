package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;

import java.time.LocalDate;

public class Fortepian extends Instrument{
    public Fortepian(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }
    public String dzwiek(){
        return "pum pUM PUM";
    }
}
