package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;

import java.time.LocalDate;
import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList<>();
        orkiestra.add(new Fortepian("Yamaha", LocalDate.of(1980,1,27)));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(1990,3,11)));
        orkiestra.add(new Flet("Yamaha", LocalDate.of(2010,7,8)));
        for(Instrument i: orkiestra){
            System.out.print(i.dzwiek()+" ");
            System.out.println(i);
        }
        System.out.println(orkiestra.get(0).getRokProdukcji());
    }
}