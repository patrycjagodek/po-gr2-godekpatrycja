package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;

import java.time.LocalDate;
public class Skrzypce extends Instrument{
    public Skrzypce(String producent, LocalDate rokProdukcji){
        super(producent, rokProdukcji);
    }
    public String dzwiek(){
        return "wzii wziiii";
    }
}
