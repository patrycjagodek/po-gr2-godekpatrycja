package pl.edu.uwm.wmii.godekpatrycja.laboratorium08.imiajd.godek;
import java.time.LocalDate;
public abstract class Osoba {
    private String nazwisko;
    private String[] imiona;
    private LocalDate DataUrodzenia;
    private boolean plec;
    public Osoba(String nazwisko, String[] imiona, LocalDate DataUrodzenia, boolean plec)
    {
        this.nazwisko = nazwisko;
        this.imiona=imiona;
        this.DataUrodzenia = DataUrodzenia;
        this.plec=plec;
    }

    public abstract String getOpis();

    public String getNazwisko()
    {
        return nazwisko;
    }

    public String[] getImiona() {
        return imiona;
    }

    public LocalDate getDataUrodzenia() {
        return DataUrodzenia;
    }

    public boolean isPlec() {
        return plec;
    }
}