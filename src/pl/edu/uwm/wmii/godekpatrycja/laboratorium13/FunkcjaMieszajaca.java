package pl.edu.uwm.wmii.godekpatrycja.laboratorium13;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;

public class FunkcjaMieszajaca{
    HashMap<Integer, HashSet<String>> h;

    public FunkcjaMieszajaca(String plik) throws FileNotFoundException {
        this.h = new HashMap<>();
        Scanner s = new Scanner(new File(plik));
        while (s.hasNext()) {
            String slowo = s.next();
            boolean temp = false;
            for (int k : this.h.keySet()) {
                if (h.hashCode() == k) {
                    temp = true;
                    break;
                }
            }
            if (!temp) {
                this.h.put(slowo.hashCode(), new HashSet<>());
            }
            this.h.get(slowo.hashCode()).add(slowo);
        }
        this.wypisz();
    }

    public void wypisz() {
        for (int k : this.h.keySet()) {
            if (this.h.get(k).size() > 1) {
                System.out.print(k + ":");
                for (String slowo : this.h.get(k)) {
                    System.out.print(" " + slowo);
                }
                System.out.println();
            }
        }
    }
}