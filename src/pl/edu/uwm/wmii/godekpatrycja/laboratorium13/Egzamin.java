package pl.edu.uwm.wmii.godekpatrycja.laboratorium13;

import java.util.TreeMap;

public class Egzamin{
    public Egzamin(){
        this.oceny=new TreeMap<>();
        this.identyfikatory=new TreeMap<>();
    }
    public void dodaj(Student s, String ocena){
        this.oceny.put(s,ocena);
        this.identyfikatory.put(s.getId(),s);
    }
    public void usun(int Id){
        this.oceny.remove(identyfikatory.get(Id));
    }

    public void zmien(int Id, String ocena){
        this.oceny.replace(identyfikatory.get(Id), ocena);
    }

    public void wypisz(){
        for (Student stu: this.oceny.keySet()){
            System.out.println(stu.getNazwisko()+" "+stu.getImie()+" "+stu.getId()+" : "+this.oceny.get(stu));
        }
        System.out.println();
    }

    private TreeMap<Student,String> oceny;
    private TreeMap<Integer,Student> identyfikatory;

    public static void main(String[] args) {
        Egzamin stu= new Egzamin();
        stu.dodaj(new Student("Patrycja","Godekk",1),"db");
        stu.dodaj(new Student("Patrycja","Godek",2),"dst");
        stu.wypisz();
        stu.zmien(2, "bdb+");
        stu.wypisz();
        stu.usun(1);
        stu.wypisz();
    }
}