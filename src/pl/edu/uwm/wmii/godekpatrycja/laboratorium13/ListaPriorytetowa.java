package pl.edu.uwm.wmii.godekpatrycja.laboratorium13;

import java.util.*;

public class ListaPriorytetowa {
    public PriorityQueue<Zadanie> lista;

    public ListaPriorytetowa(){
        this.lista = new PriorityQueue<>();
    }

    public void uruchom() {
        Scanner s = new Scanner(System.in);
        String[] polecenia;
        String line = s.nextLine();
        Iterator<String> iter;
        while (!line.equals("zakoncz")) {
            polecenia = line.split(" ");
            iter = Arrays.stream(polecenia).iterator();
            if (iter.hasNext()) {
                iter.next();
                if (polecenia[0].equals("dodaj")) {
                    System.out.println("dodano");
                    int priorytet = 0;
                    StringBuffer opis = new StringBuffer();
                    if (iter.hasNext()) {
                        priorytet = Integer.parseInt(iter.next());
                    }
                    while (iter.hasNext()) {
                        opis.append(iter.next());
                        opis.append(" ");
                    }
                    lista.add(new Zadanie(priorytet, opis.toString()));
                } else if (polecenia[0].equals("nastepne")) {
                    lista.remove();
                    System.out.println("usunieto");
                } else {
                    System.out.println("nie ma takiego polecenia");
                }
            }
            line = s.nextLine();

        }
    }

    public void wypisz() {
        PriorityQueue<Zadanie> kopia = new PriorityQueue<>(this.lista);
        while (!kopia.isEmpty()) {
            Zadanie e = kopia.remove();
            System.out.println("Priorytet: " + e.priorytet);
            System.out.println("Opis: " + e.opis);
        }
    }


    public static void main(String[] args) {
        ListaPriorytetowa LP = new ListaPriorytetowa();
        LP.uruchom();
        LP.wypisz();
    }

    class Zadanie implements Comparable<Zadanie> {
        public Zadanie(int priority, String opis) {
            this.opis = opis;
            this.priorytet = priority;
        }

        int priorytet;
        String opis;

        @Override
        public int compareTo(Zadanie o) {
            return Integer.compare(this.priorytet, o.priorytet);
        }
    }
}