package pl.edu.uwm.wmii.godekpatrycja.laboratorium13;

public class Student implements Comparable<Student>{
    public Student(String imie, String nazwisko, int id){
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.id = id;
    }

    public String getImie() {
        return imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public int getId() {
        return id;
    }

    private String imie;
    private String nazwisko;
    private int id;

    @Override
    public int compareTo(Student s) {
        if(this.nazwisko.compareTo(s.getNazwisko())<=0){
            if(this.imie.compareTo(s.getImie())<=0){
                if(this.id<s.getId()){
                    return -1;
                }
                else if(this.id==s.getId()){
                    return 0;
                }
                return 1;
            }
            return this.imie.compareTo(s.getImie());
        }
        return this.nazwisko.compareTo(s.getNazwisko());
    }
}
