package pl.edu.uwm.wmii.godekpatrycja.laboratorium12;
import java.util.LinkedList;
import java.util.Stack;
import java.util.*;

public class lab12 {
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        for(int i=n-1; i<pracownicy.size();i+=n-1){
            pracownicy.remove(i);
        }
    }

    public static <T> void odwroc(LinkedList<T> lista){
        LinkedList<T> temp = new LinkedList<>(lista);
        for(int i = lista.size()-1, j=0; i >= 0; i--, j++){
            lista.set(j,temp.get(i));
        }
    }

    // BRAK ZADANIA 5 !!!!!!!!!!!!!!!!

    public static void liczby(int x){
        Stack<Integer> cyfry = new Stack<>();
        while(x!=0){
            cyfry.push(x%10);
            x/=10;
        }
        while (!cyfry.empty()){
            System.out.print(cyfry.pop()+" ");
        }
        System.out.println();
    }

    public static void Erastotenes(int n){
        boolean[] prime = new boolean[n+1];
        for(int i = 0 ; i < n; i++)
            prime[i] = true;

        for(int p = 2; p*p <=n; p++)
        {
            if(prime[p])
            {
                for(int i = p*p; i <= n; i += p)
                    prime[i] = false;
            }
        }
        for(int i = 2; i <= n; i++)
        {
            if(prime[i])
                System.out.print(i + " ");
        }
    }

    public static <T extends Iterable<?>> void print(T x ){
        Iterator<?> it = x.iterator();
        while (it.hasNext()){
            System.out.print(it.next());
            if (it.hasNext()){
                System.out.print(", ");
            }
        }
        System.out.println();
    }
}
