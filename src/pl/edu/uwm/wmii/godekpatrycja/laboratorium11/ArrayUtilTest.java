package pl.edu.uwm.wmii.godekpatrycja.laboratorium11;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class ArrayUtilTest {
    public static void main(String[] args) {
        ArrayList<LocalDate> Local_Date = new ArrayList<>();
        Local_Date.add(LocalDate.of(1996,1,10));
        Local_Date.add(LocalDate.of(1997,2,11));
        Local_Date.add(LocalDate.of(1997,2,1));
        Local_Date.add(LocalDate.of(1997,3,1));
        System.out.println(ArrayUtil.isSorted(Local_Date));
        Collections.sort(Local_Date);
        System.out.println(ArrayUtil.isSorted(Local_Date));
        ArrayList<Integer> tab = new ArrayList<>();
        tab.add(1);
        tab.add(6);
        tab.add(2);
        tab.add(1);
        System.out.println(ArrayUtil.isSorted(tab));
        Collections.sort(tab);
        System.out.println(ArrayUtil.isSorted(tab)+"\n");
        ArrayUtil.selectionSort(Local_Date);
        ArrayUtil.selectionSort(tab);
        ArrayUtil.mergeSort(Local_Date);
        ArrayUtil.mergeSort(tab);
        System.out.println(Local_Date);
        System.out.println(ArrayUtil.binSearch(Local_Date,LocalDate.of(2000,1,1)));
        System.out.println(ArrayUtil.binSearch(Local_Date,LocalDate.of(1997,2,1))+"\n");
        System.out.println(tab);
        System.out.println(ArrayUtil.binSearch(tab,6));
        System.out.println(ArrayUtil.binSearch(tab,50));
    }
}