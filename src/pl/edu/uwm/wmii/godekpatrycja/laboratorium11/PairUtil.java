package pl.edu.uwm.wmii.godekpatrycja.laboratorium11;

public class PairUtil<T> {
    public static <T> Pair<T> swap(Pair<T> para){
        Pair<T> prev = new Pair<>(para.getFirst(), para.getSecond());
        para.swap();
        return prev;
    }
}
