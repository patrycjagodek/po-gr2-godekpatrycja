package pl.edu.uwm.wmii.godekpatrycja.laboratorium11;

import java.util.ArrayList;
import java.util.Collections;

public class ArrayUtil {
    public static <T extends Comparable<? super T>> boolean isSorted(ArrayList<T> tab){
        ArrayList<T> temp = new ArrayList<>(tab);
        Collections.sort(temp);
        return tab.equals(temp);
    }
    public static <T extends Comparable<? super T>> int binSearch(ArrayList<T> tab, T x){
        if(ArrayUtil.isSorted(tab)){
            int l = 0;
            int r = tab.size() - 1;
            while (l <= r) {
                int m = l + (r - l) / 2;
                if (tab.get(m).equals(x))
                    return m;
                if (tab.get(m).compareTo(x) < 0)
                    l = m + 1;
                else
                    r = m - 1;
            }
        }
        return -1;
    }
    public static <T extends Comparable<? super T>> void selectionSort(ArrayList<T> tab){
        int x;
        T temp;
        for (int i = tab.size()-1; i >= 0 ; i--)
        {
            x = 0;
            for (int j = 0; j <= i; j++)
            {
                if (tab.get(x).compareTo(tab.get(j)) < 0)
                {
                    x = j;
                }
            }

            if (x != i)
            {
                temp = tab.get(i);
                tab.set(i, tab.get(x));
                tab.set(x, temp);
            }
        }
    }
    public static <T extends Comparable<? super T>>  void mergeSort(ArrayList<T> tab){
        if (tab.size() > 1) {
            ArrayList<T> l = new ArrayList<>();
            ArrayList<T> r = new ArrayList<>();
            boolean p = true;
            while (!tab.isEmpty()) {
                if (p) {
                    l.add(tab.remove(0));
                } else {
                    r.add(tab.remove(tab.size()/2));
                }
                p = !p;
            }
            mergeSort(l);
            mergeSort(r);
            while (!l.isEmpty() && !r.isEmpty()) {
                if(l.get(0).compareTo(r.get(0)) <= 0){
                    tab.add(l.remove(0));
                }
                else {
                    tab.add(r.remove(0));
                }
            }
            if(!l.isEmpty()){
                tab.addAll(l);
            }
            else if (!r.isEmpty()){
                tab.addAll(r);
            }
        }
    }
}
