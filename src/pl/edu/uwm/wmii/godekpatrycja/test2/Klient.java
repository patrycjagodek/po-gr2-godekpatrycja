package pl.edu.uwm.wmii.godekpatrycja.test2;

import java.time.LocalDate;

public class Klient implements Cloneable, Comparable<Klient> {
    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id, LocalDate dataZakupy, double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = dataZakupy;
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupy() {
        return dataZakupy;
    }

    public double getRachunek() {
        return rachunek;
    }

    @Override
    public int compareTo(Klient o){
        if(dataZakupy.compareTo(o.dataZakupy)>0){
            return 1;
        }
        if(dataZakupy.compareTo(o.dataZakupy)<0){
            return -1;
        }
        if(nazwa.compareTo(o.nazwa)>0){
            return 1;
        }
        if(nazwa.compareTo(o.nazwa)<0){
            return -1;
        }
        if(rachunek>o.rachunek){
            return 1;
        }
        if(rachunek<o.rachunek){
            return -1;
        }
        return 0;
    }

    @Override
    public String toString(){
        return getNazwa() + ", " + getId() + ", " + getDataZakupy() + ", " + getRachunek();
    }
}
