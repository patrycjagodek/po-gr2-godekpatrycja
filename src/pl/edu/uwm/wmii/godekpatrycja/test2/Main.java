package pl.edu.uwm.wmii.godekpatrycja.test2;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

public class Main {
    public static void main(String[] args) {
        ArrayList<Klient> klienci = new ArrayList<>();
        klienci.add(new Klient("Godek", 1, LocalDate.of(2020, 2, 15), 250));
        klienci.add(new Klient("Baran", 2, LocalDate.of(2020, 2, 15), 600));
        klienci.add(new Klient("Nowak", 3, LocalDate.of(2020, 6, 24), 25));
        klienci.add(new Klient("Kot", 4, LocalDate.of(2020, 3, 2), 25));
        klienci.add(new Klient("Kowalski", 5, LocalDate.of(2020, 6, 24), 100));
        klienci.add(new Klient("Kowalski", 6, LocalDate.of(2020, 3, 15), 9000));
        for (Klient klient : klienci) {
            System.out.println(klient.toString());
        }
        Collections.sort(klienci);
        System.out.println("------\nPo sortowaniu:");
        for (Klient klient : klienci) {
            System.out.println(klient.toString());
        }

        Obslugaklienta.setProcentRabatu();
        System.out.println("------\nZ rabatem:");
        for(Klient k:klienci){
            System.out.println(k.getNazwa() + ", " + k.getId() +", Rabat: "+discountAmount(k));
        }

        System.out.println("-------\nDiscount Map:");
        HashMap<Integer,String> map = DiscountMap(klienci);
        for (Integer k:map.keySet()){
//            System.out.println("id klienta: " + k + ", nazwisko klienta: " + map.get(k)); // jezeli mapa wyswietla nazwisko klienta
            System.out.println("id klienta: " + k + ", rabat: " + map.get(k)); // jezeli mapa wyswietla rabat
        }
    }

    public static double discountAmount(Klient k){
        double kwota_rabatu = 0;
        if (k.getRachunek()>300){
            kwota_rabatu = k.getRachunek()*Obslugaklienta.procentRabatu;
        }
        return kwota_rabatu;
    }

    public static HashMap<Integer,String> DiscountMap(ArrayList<Klient> klist) {
        HashMap<Integer,String> discmap = new HashMap<>();
        for(Klient k : klist)
            if(discountAmount(k)!=0)
//                discmap.put(k.getId(), k.getNazwa()); // jezeli mapa ma wyswietlac nazwisko klienta jako wartosc
                discmap.put(k.getId(), String.valueOf(discountAmount(k))); // jezeli mapa ma wyswietlac rabat jako wartosc
        return discmap;
    }
}
