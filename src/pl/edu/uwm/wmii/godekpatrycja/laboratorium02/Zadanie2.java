package pl.edu.uwm.wmii.godekpatrycja.laboratorium02;
import java.util.Scanner;
import java.util.Random;


public class Zadanie2 {
    public static void generuj(int tab[], int n, int min, int max){
        Random random = new Random();
        for(int i = 0; i < n; i++){
            tab[i] = random.nextInt(max-min)+min;
        }
    }

    public static int ileNieparzystych(int tab[]){
        System.out.println("a)");
        int ile = 0;
        for(int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 != 0 && tab[i] != 0) {
                ile++;
            }
        }
        return ile;
    }

    public static int ileParzystych(int tab[]){
        int ile = 0;
        for(int i = 0; i < tab.length; i++) {
            if (tab[i] % 2 == 0 && tab[i] != 0) {
                ile++;
            }
        }
        return ile;
    }

    public static int ileDodatnich(int tab[]){
        System.out.println("b)");
        int ile = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i]>0)
                ile++;
        }
        return ile;
    }

    public static int ileUjemnych(int tab[]){
        int ile = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i]<0)
                ile++;
        }
        return ile;
    }

    public static int ileZerowych(int tab[]){
        int ile = 0;
        for(int i = 0; i < tab.length; i++){
            if(tab[i] == 0)
                ile++;
        }
        return ile;
    }

    public static int ileMaksymalnych(int tab[]){
        System.out.println("c)");
        int naj = 0;
        int ile = 0;
        for(int i = 0; i < tab.length; i++) {
            if (tab[i] > naj) {
                naj = tab[i];
            }
        }
        for(int i = 0; i < tab.length; i++){
            if(tab[i]==naj){
                ile++;
            }
        }
        return ile;
    }

    public static int sumaDodatnich(int tab[]){
        System.out.println("d)");
        int sumadod = 0;
        for(int i = 0; i < tab.length; i++) {

            if(tab[i] > 0){
                sumadod += tab[i];
            }
        }
        return sumadod;
    }

    public static int sumaUjemnych(int tab[]){
        int sumauj = 0;
        for(int i = 0; i < tab.length; i++) {

            if(tab[i] < 0){
                sumauj += tab[i];
            }
        }
        return sumauj;
    }

    public static int dlugoscMakskymalnegoCiaguDodatnich(int tab[]){
        System.out.println("e) ");
        int dodatnie = 0;
        int dlugosc = 0;
        for(int i = 0;i < tab.length; i++) {
            if (tab[i] > 0) {
                dodatnie++;
            }
            if (dodatnie > dlugosc) {
                dlugosc = dodatnie;
            }
            if (tab[i] <= 0) {
                dodatnie = 0;
            }
        }
        return dlugosc;
    }

        public static void signum(int tab[]){
        System.out.println("f)");
        int tab1[]=new int[tab.length];
        for(int i = 0; i < tab.length; i++) {
            tab1[i]= tab[i];
        }
        for(int i = 0; i < tab.length; i++) {
            if (tab1[i] > 0) {
                tab1[i] = 1;
            }
            if (tab1[i] < 0) {
                tab1[i] = -1;
            }
            System.out.println(tab1[i]);
        }
    }

    public static void odwrocFragment(int tab[], int lewy, int prawy){
        int tab2[]=new int[tab.length];
        for(int i = 0; i < tab.length; i++) {
            tab2[i]= tab[i];
        }
        for(int i = 0;i < prawy-lewy+1; i++) {
            tab2[lewy+i]=tab[prawy-i];
        }
        System.out.println("\n");
        for(int i = 0;i < tab.length; i++) {
            System.out.println(tab2[i]);
        }
    }

    public static void main(String[] args) {
        int min = -999;
        int max = 999;
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Podaj ile: ");
        n = s.nextInt();
        int[] tab = new int[n];
        generuj(tab, n, min, max);
        for(int i = 0; i < n; i++){
            System.out.println(tab[i]);
        }
        System.out.println(ileNieparzystych(tab));
        System.out.println(ileParzystych(tab));
        System.out.println(ileDodatnich(tab));
        System.out.println(ileUjemnych(tab));
        System.out.println(ileZerowych(tab));
        System.out.println(ileMaksymalnych(tab));
        System.out.println(sumaDodatnich(tab));
        System.out.println(sumaUjemnych(tab));
        System.out.println(dlugoscMakskymalnegoCiaguDodatnich(tab));
        signum(tab);
        int lewy;
        int prawy;
        Scanner le = new Scanner(System.in);
        System.out.print("Podaj lewy: ");
        lewy = le.nextInt();
        Scanner pr = new Scanner(System.in);
        System.out.print("Podaj prawy: ");
        prawy = pr.nextInt();
        odwrocFragment(tab, lewy, prawy);
    }
}