package pl.edu.uwm.wmii.godekpatrycja.laboratorium02;
import java.util.Scanner;
import java.util.Random;


public class Zadanie1 {

    public static void main(String[] args) {
        int n;
        int min = -999;
        int max = 999;
        Scanner s = new Scanner(System.in);
        System.out.print("Ile liczb? (od 1 do 100): ");
        n = s.nextInt();
        Random random = new Random();
        int[] tab = new int[n];
        for(int i = 0; i < n; i++){
            tab[i] = random.nextInt(max-min)+min;
        }
        for(int i = 0; i < n; i++){
            System.out.println(tab[i]);
        }

        System.out.println("a) ");
        int p = 0;
        int np = 0;
        for(int i = 0; i < n; i++){
            if(tab[i]%2==0 && tab[i]!=0){
                p++;
            }
            else{
                np++;
            }
        }
        System.out.print("Parzystych: " + p + "\nNieparzystych: "+ np);
        System.out.println("\nb) ");
        int uj = 0;
        int dod = 0;
        int zero = 0;
        for(int i = 0; i < n; i++){
            if(tab[i]>0){
                dod++;
            }
            else if(tab[i]<0){
                uj++;
            }
            else{
                zero++;
            }
        }
        System.out.print("Ujemnych: " + uj + "\nDodatnich: "+ dod + "\nZero: " + zero);
        System.out.println("\nc) ");
        int naj = 0;
        int ile = 0;
        for(int i = 0; i < n; i++) {
            if (tab[i] > naj) {
                naj = tab[i];
            }
        }
        for(int i = 0; i < n; i++){
            if(tab[i]==naj){
                ile++;
            }

        }
        System.out.print("Najwiekszy element: " + naj + "\nIle razy: "+ ile);
        System.out.println("\nd) ");
        int sumauj = 0;
        int sumadod = 0;
        for(int i = 0; i < n; i++) {
            if (tab[i] < 0) {
                sumauj += tab[i];
            }
            if(tab[i] > 0){
                sumadod += tab[i];
            }
        }
        System.out.print("Suma ujemnych: " + sumauj + "\nSuma dodatnich: "+ sumadod);
        System.out.println("\ne) ");
        int dodatnie = 0;
        int dlugosc = 0;
        for(int i = 0;i < n; i++) {
            if (tab[i] > 0) {
                dodatnie++;
            }
            if (dodatnie > dlugosc) {
                dlugosc = dodatnie;
            }
            if (tab[i] <= 0) {
                dodatnie = 0;
            }
        }
        System.out.print("Najdluzszy fragment: " + dlugosc);
        System.out.println("\nf) ");
        int tab1[]=new int[n];
        for(int i = 0; i < n; i++) {
            tab1[i]= tab[i];
        }
        for(int i = 0; i < n; i++) {
            if (tab1[i] > 0) {
                tab1[i] = 1;
            }
            if (tab1[i] < 0) {
                tab1[i] = -1;
            }
            System.out.println(tab1[i]);
        }
        System.out.println("\ng) ");
        int lewy;
        int prawy;
        int tab2[]=new int[n];
        for(int i = 0; i < n; i++) {
            tab2[i]= tab[i];
        }
        Scanner le = new Scanner(System.in);
        System.out.print("Podaj lewy: ");
        lewy = le.nextInt();
        Scanner pr = new Scanner(System.in);
        System.out.print("Podaj prawy: ");
        prawy = pr.nextInt();
        for(int i = 0;i < prawy-lewy+1; i++) {
            tab2[lewy+i]=tab[prawy-i];
        }
        System.out.println("\n");
        for(int i = 0;i < n; i++) {
            System.out.println(tab2[i]);
        }
    }
}