package pl.edu.uwm.wmii.godekpatrycja.laboratorium00;

public class Zadanie11 {
    static public void main(String[] args){
    System.out.println("Dar\n" +
            "\n" +
            "Dzień taki szczęśliwy.\n" +
            "Mgła opadła wcześnie, pracowałem w ogrodzie.\n" +
            "Kolibry przystawały nad kwiatem kaprifolium.\n" +
            "Nie było na ziemi rzeczy, którą chciałbym mieć.\n" +
            "Nie znałem nikogo, komu warto byłoby zazdrościć.\n" +
            "Co przydarzyło się złego, zapomniałem.\n" +
            "Nie wstydziłem się myśleć, że byłem kim jestem.\n" +
            "Nie czułem w ciele żadnego bólu.\n" +
            "Prostując się, widziałem niebieskie morze i żagle.\n");
    }
}
