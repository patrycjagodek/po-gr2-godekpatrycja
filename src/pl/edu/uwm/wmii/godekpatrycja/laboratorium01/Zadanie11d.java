package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11d {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        float wynik = 0;
        for (int i = -2; i <= n; i++) {
            System.out.println(Math.sqrt(Math.abs(i)));
            wynik += Math.sqrt(Math.abs(i));
        }
        System.out.println(" = " + wynik);
    }
}