package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie2_2 {
    private static float foo(float[] n) {
        float suma = 0;
        float wynik = 0;
        for (int i = 0; i < n.length; i++) {
            if (n[i] >= 0)
                suma += n[i];
        }
        wynik = 2 * suma;
        return wynik;
    }

    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Ile liczb?");
        n = s.nextInt();
        float[] a = new float[n];
        System.out.println("Wprowadz liczby:");
        for (int i = 0; i < n; i++) {
            a[i] = s.nextFloat();
        }

        System.out.println(foo(a));

    }
}