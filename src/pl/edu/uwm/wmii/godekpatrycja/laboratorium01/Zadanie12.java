package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie12 {
    public static void fun(int[] x)
    {
        for (int i = 1; i < x.length; i++) {
            System.out.print(x[i]);
        }
        System.out.print(x[0]);
    }

    public static void main(String[] args) {
        int[] tab = {1, 2, 3, 4};
        fun(tab);
    }
}