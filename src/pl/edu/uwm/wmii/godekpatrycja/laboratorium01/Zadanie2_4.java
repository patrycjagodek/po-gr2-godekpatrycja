package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie2_4 {
    private static void foo(float[] n) {
        float min = n[0];
        float max = n[0];
        for(int i = 0; i < n.length; i++)
        {
            if(max < n[i])
            {
                max = n[i];
            }
            if(min > n[i])
            {
                min = n[i];
            }
        }
        System.out.format("Liczb najmniejsza: %f, a najwieszka: %f", min, max);
    }

    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Ile liczb?");
        n = s.nextInt();
        float[] a = new float[n];
        System.out.println("Wprowadz liczby:");
        for (int i = 0; i < n; i++) {
            a[i] = s.nextFloat();
        }

        foo(a);

    }
}