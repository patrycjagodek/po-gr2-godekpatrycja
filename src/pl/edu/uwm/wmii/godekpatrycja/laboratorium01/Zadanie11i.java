package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11i {

    private static int silnia(int x)
    {
        if (x < 1)
            return 1;
        else
            return x * silnia(x - 1);
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        float wynik = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println(i);
            wynik += (Math.pow(-1,i)*i)/silnia(i);
        }
        System.out.println(" = " + wynik);
    }
}