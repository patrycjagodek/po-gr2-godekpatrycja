package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11g {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        int wynikd = 0;
        int wynikm = 1;
        for (int i = 1; i <= n; i++) {
            System.out.println(i);
            wynikd += i;
            wynikm *= i;
        }
        System.out.format("Wynik dodawania to: %d, mnozenia to %d", wynikd, wynikm);
    }
}