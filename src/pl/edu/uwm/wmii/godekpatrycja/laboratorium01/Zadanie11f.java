package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11f {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        int wynik = 0;
        for (int i = 1; i <= n; i++) {
            System.out.println(Math.pow(i,2));
            wynik += (Math.pow(i,2));
        }
        System.out.println(" = " + wynik);
    }
}