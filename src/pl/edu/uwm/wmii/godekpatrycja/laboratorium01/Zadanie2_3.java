package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie2_3 {
    private static void foo(float[] n) {
        int iled = 0;
        int ileu = 0;
        int ilez = 0;
        for (int i = 0; i < n.length; i++) {
            if (n[i] > 0) {
                iled++;
            }
            else if (n[i] < 0) {
                ileu++;
            }
            else{
                ilez++;
            }
        }
        System.out.format("Liczb dodatnich jest %d, ujemnych %d, a zer %d", iled,ileu,ilez);
    }

    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Ile liczb?");
        n = s.nextInt();
        float[] a = new float[n];
        System.out.println("Wprowadz liczby:");
        for (int i = 0; i < n; i++) {
            a[i] = s.nextFloat();
        }

        foo(a);

    }
}