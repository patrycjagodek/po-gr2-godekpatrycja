package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie2_5 {
    private static int foo(float[] n) {
        int pary = 0;
        for (int i = 0; i < n.length-1; i++) {
            if(n[i]>0&&n[i+1]>0)
            {

                pary++;
            }
        }
        return pary;
    }

    public static void main(String[] args) {
        int n;
        Scanner s = new Scanner(System.in);
        System.out.print("Ile liczb?");
        n = s.nextInt();
        float[] a = new float[n];
        System.out.println("Wprowadz liczby:");
        for (int i = 0; i < n; i++) {
            a[i] = s.nextFloat();
        }

        System.out.println(foo(a));

    }
}