package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11c {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        int wynik = 0;
        for (int i = -3; i <= n; i++) {
            System.out.println(i);
            wynik += Math.abs(i);
        }
        System.out.println(" = " + wynik);
    }
}