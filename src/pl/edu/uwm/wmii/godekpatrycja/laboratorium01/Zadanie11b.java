package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie11b {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n: ");
        int n = scan.nextInt();
        int wynik = 1;
        for (int i = 1; i <= n; i++) {
            System.out.println(i);
            wynik *= i;
        }
        System.out.println(" = " + wynik);
    }
}