package pl.edu.uwm.wmii.godekpatrycja.laboratorium01;
import java.util.Scanner;

public class Zadanie2_1 {

    private static int silnia(int x)
    {
        if (x < 1)
            return 1;
        else
            return x * silnia(x - 1);
    }

    private static int a(int[] n)
    {
        int ile = 0;
        for(int i = 0; i < n.length ; i++)
        {
            if(n[i] % 2 != 0)
                ile++;
        }
    return ile;
    }

    private static int b(int[] n) {
        int ile = 0;
        for (int i = 0; i < n.length; i++) {
            if (n[i] % 3 == 0 && n[i] % 5 != 0)
                ile++;
        }
        return ile;
    }
        private static int c(int[] n)
        {
            int ile = 0;
            for(int i = 0; i < n.length ; i++)
            {
                if(Math.pow(n[i],2)%2 == 0)
                    ile++;
            }
            return ile;
        }

    private static int d(int[] n)
    {
        int ile = 0;
        for(int i = 1; i < n.length-1 ; i++)
        {
            if(n[i]<((n[i-1]+n[i+1])/2))
                ile++;
        }
        return ile;
    }

    private static int e(int[] n)
    {
        int ile = 0;
        for(int i = 1; i < n.length ; i++)
        {
            if(Math.pow(2,n[i])<  n[i] && n[i] < silnia(i))
                ile++;
        }
        return ile;
    }

    private static int f(int[] n)
    {
        int ile = 0;
        for(int i = 1; i < n.length ; i++)
        {
            if(n[i-1]%2==0 && i%2!=0)
                ile++;
        }
        return ile;
    }

    private static int g(int[] n)
    {
        int ile = 0;
        for(int i = 0; i < n.length ; i++)
        {
            if(n[i]>=0 && n[i]%2!=0)
                ile++;
        }
        return ile;
    }

    private static int h(int[] n)
    {
        int ile = 0;
        for(int i = 1; i < n.length ; i++)
        {
            if(Math.abs(n[i-1])<Math.pow(i,2))
                ile++;
        }
        return ile;
    }


    public static void main(String[] args) {
        int n;

        Scanner s = new Scanner(System.in);

        System.out.print("Ile liczb?");

        n = s.nextInt();

        int[] a = new int[n];

        System.out.println("Wprowadz liczby:");

        for(int i = 0; i < n; i++)

        {
            a[i] = s.nextInt();
        }

        System.out.println(" ile nieparzystych: " + a(a));
        System.out.println(" ile podzielnych przez 3 i niepodzielnych przez 5: " + b(a));
        System.out.println(" ile jest kwadratami liczby parzystej: " + c(a));
        System.out.println(" d): " + d(a));
        System.out.println(" e): " + e(a));
        System.out.println(" f): " + f(a));
        System.out.println(" g): " + g(a));
        System.out.println(" h): " + h(a));
    }
}