package pl.edu.uwm.wmii.godekpatrycja.laboratorium05;

import java.util.ArrayList;
import java.util.Arrays;

public class Zadanieod1do5 {

    public static ArrayList<Integer> append(ArrayList<Integer> tab1, ArrayList<Integer> tab2){
        ArrayList<Integer> tabL = new ArrayList<>();
        tabL.addAll(tab1);
        tabL.addAll(tab2);
        return tabL;
    }

    public static ArrayList<Integer> merge(ArrayList<Integer> tab1, ArrayList<Integer> tab2){
        int min=Math.min(tab1.size(), tab2.size());
        int max=Math.max(tab1.size(), tab2.size());
        ArrayList<Integer> tabL =new ArrayList<>();
        for (int i=0; i<min;i++)
        {
            tabL.add(tab1.get(i));
            tabL.add(tab2.get(i));
        }
        for (int i=min; i<max;i++)
        {
            if (tab1.size()>tab2.size())
            {
                tabL.add(tab1.get(i));
            }
            else
            {
                tabL.add(tab2.get(i));
            }
        }
        return tabL;
    }

    //public static ArrayList<Integer> mergeSorted(ArrayList<Integer> tab1, ArrayList<Integer> tab2)
        // brak pomyslu

    public static ArrayList<Integer> reversed(ArrayList<Integer> tab1){
        ArrayList<Integer> tabL = new ArrayList<>();
        for(int i=tab1.size()-1;i>=0;i--)
        {
            tabL.add(tab1.get(i));
        }
        return tabL;
    }

    public static void reverse(ArrayList<Integer> tab1){
        ArrayList<Integer> temp= new ArrayList<>(tab1);
        int x=0;
        for(int i=temp.size()-1; i >= 0; i--) {
            tab1.set(x,temp.get(i));
            x++;
        }

            System.out.println(tab1);
    }

    public static void main(String[] args) {
        ArrayList<Integer> tab1= new ArrayList<>(Arrays.asList(4,3,2,8));
        ArrayList<Integer> tab2= new ArrayList<>(Arrays.asList(1,7,5,6,3));
        System.out.println(tab1);
        System.out.println(tab2);
//        System.out.println(append(tab1,tab2));
//        System.out.println(merge(tab1, tab2));
//        System.out.println(reversed(tab1));
        reverse(tab1);

    }
}
