package pl.edu.uwm.wmii.godekpatrycja.laboratorium04;
import java.math.BigInteger;


public class Zadanie4 {

    public static BigInteger zad4(int n){
        int pola=n*n;
        BigInteger suma = new BigInteger("0");
        BigInteger w = new BigInteger("2");
        for (int i=0;i<pola;i++)
        {
            suma=suma.add(w.pow(i));
        }
        return suma;
    }

    public static void main(String[] args) {
        System.out.println(zad4(3));
    }
}
