package pl.edu.uwm.wmii.godekpatrycja.laboratorium04;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class Zadanie5 {
    public static BigDecimal ile(int k, double p, int n){
        BigDecimal kapital = new BigDecimal(String.valueOf(k));
        BigDecimal stopaprocentowa = new BigDecimal(String.valueOf(p));
        for (int i=0;i<n;i++)
        {
            kapital = kapital.add(kapital.multiply(stopaprocentowa));
        }
        return kapital.setScale(2, RoundingMode.HALF_UP);
    }

    public static void main(String[] args) {
        System.out.println(ile(1000, 0.08, 20));
    }
}
