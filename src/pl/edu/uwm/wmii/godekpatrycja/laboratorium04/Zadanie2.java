package pl.edu.uwm.wmii.godekpatrycja.laboratorium04;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Zadanie2 {

    public static int wystapienia(String nazwapliku, char znak) {
        int ile = 0;
        try{
            File plik = new File(nazwapliku);
            Scanner s = new Scanner(plik);
            while (s.hasNextLine()) {
                String linia = s.nextLine();
                ile += Zadanie1.countChar(linia, znak);
            }
            s.close();
        }
        catch (FileNotFoundException brak)
    {
        System.out.println("Brak pliku.");
        brak.printStackTrace();
    }
        System.out.println(ile);
        return ile;
    }
    public static void main(String[] args) {
        System.out.print("Aktualnie w pliku: 'Ala ma kota ala' i zlicza 'a'\n");
        wystapienia("src/pl/edu/uwm/wmii/godekpatrycja/laboratorium04/plik.txt", 'a');
    }
}
