package pl.edu.uwm.wmii.godekpatrycja.laboratorium04;
import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;

public class Zadanie3 {

    public static int ilewyrazow(String nazwapliku, String wyraz) {
        int ile = 0;
        try{
            File plik = new File(nazwapliku);
            Scanner s = new Scanner(plik);
            while (s.hasNextLine()) {
                String linia = s.nextLine();
                ile += Zadanie1.countSubstr(linia, wyraz);
            }
            s.close();
        }
        catch (FileNotFoundException brak)
        {
            System.out.println("Brak pliku.");
            brak.printStackTrace();
        }
        System.out.println(ile);
        return ile;
    }
    public static void main(String[] args) {
        System.out.print("Aktualnie w pliku: 'ala ma kota ala' i zlicza 'ala'\n");
        ilewyrazow("src/pl/edu/uwm/wmii/godekpatrycja/laboratorium04/plik.txt", "ala");
    }
}
