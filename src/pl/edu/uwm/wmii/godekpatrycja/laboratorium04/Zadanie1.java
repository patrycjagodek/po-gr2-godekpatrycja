package pl.edu.uwm.wmii.godekpatrycja.laboratorium04;
import java.util.Arrays;
import java.util.Scanner;

public class Zadanie1 {

    public static int countChar(String str, char c){
        int ile = 0;
        for(int i = 0; i < str.length();i++){
            if(str.charAt(i) == c){
                ile++;
            }
        }
        return ile;
    }


    public static int countSubstr(String str, String subStr){
        int ile=0;
        String string;
        for (int i=0;i<=str.length()-subStr.length();i++)
        {
            string=str.substring(i, subStr.length()+i);
            if (string.equals(subStr))
            {
                ile+=1;
            }
        }
        return ile;
    }

    public static String middle(String str)
    {
        if(str.length()%2==0)
        {
            return str.substring((str.length()/2)-1, (str.length()/2)+1);
        }
        else {
            return str.valueOf(str.charAt(str.length()/2));
        }
    }

    public static String repeat(String str, int n)
    {
        StringBuilder napis= new StringBuilder();
        for(int i=0;i<n;i++){
            napis.append(str);
        }
        return napis.toString();
    }

    public static int[] where(String str, String subStr)
    {
        int [] tab = new int[countSubstr(str, subStr)];
        String napis;
        int ile=0;
        for (int i=0;i<=str.length()-subStr.length();i++)
        {
            napis=str.substring(i, subStr.length()+i);
            if (napis.equals(subStr)) {
                tab[ile] = i;
                ile += 1;
            }
        }
        return tab;
    }

    public static String change(String str){
        StringBuffer z = new StringBuffer();
        for (int i=0;i<str.length();i++)
        {
            if(Character.isUpperCase(str.charAt(i)))
            {
                z.append(String.valueOf(str.charAt(i)).toLowerCase());
            }
            else
            {
                z.append(String.valueOf(str.charAt(i)).toUpperCase());
            }
        }
        return z.toString();
    }

    public static String nice(String str){
        StringBuffer napis = new StringBuffer();
        for (int i=0;i<str.length();i++)
        {
            napis.append(str.charAt(i));
            if (i%3==0)
            {
                napis.append("'");
            }
        }
        return napis.toString();
    }

    public static String niceh(String str, String sep, int ile){
        StringBuffer napis = new StringBuffer();
        for (int i=0;i<str.length();i++)
        {
            napis.append(str.charAt(i));
            if (i % ile == 0)
            {
                napis.append(sep);
            }
        }
        return napis.toString();
    }

    public static void main(String[] args) {
        Scanner s = new Scanner(System.in);
//        System.out.println("a) ");
//        System.out.println("Podaj napis: ");
//        String a = s.nextLine();
//        System.out.println("Podaj litere: ");
//        char litera = s.nextLine().charAt(0);
//        System.out.println(countChar(a, litera));
//        System.out.println("b) ");
//        System.out.println("Podaj 1 napis: ");
//        String b1 = s.nextLine();
//        System.out.println("Podaj 2 napis: ");
//        String b2 = s.nextLine();
//        System.out.println(countSubstr(b1, b2));
//        System.out.println("c) ");
//        System.out.println("Podaj napis: ");
//        String c = s.nextLine();
//        System.out.println(middle(c));
//        System.out.println("d) ");
//        System.out.println("Podaj napis: ");
//        String d = s.nextLine();
//        System.out.println("Podaj ile: ");
//        int n;
//        n = s.nextInt();
//        System.out.println(repeat(d, n));
//        System.out.println("e) ");
//        System.out.println("Podaj 1 napis: ");
//        String e1 = s.nextLine();
//        System.out.println("Podaj 2 napis: ");
//        String e2 = s.nextLine();
//        System.out.println(Arrays.toString(where(e1, e2)));
//        System.out.println("f) ");
//        System.out.println("Podaj napis: ");
//        String f = s.nextLine();
//        System.out.println(change(f));
//        System.out.println("g) ");
//        System.out.println("Podaj napis: ");
//        String g = s.nextLine();
//        System.out.println(nice(g));
        System.out.println("h) ");
        System.out.println("Podaj napis: ");
        String h = s.nextLine();
        System.out.println("Podaj separator: ");
        String sep = s.nextLine();
        System.out.println("Podaj co ile: ");
        int x;
        x = s.nextInt();
        System.out.println(niceh(h, sep,x));
    }
}
